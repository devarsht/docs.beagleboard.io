.. _beagleboneblack-home:

BeagleBone Black
###################

BeagleBone Black is a low-cost, community-supported development platform for developers and hobbyists. 
Boot Linux in under 10 seconds and get started on development in less than 5 minutes with just a single USB cable.

.. admonition:: License Terms

    * This documentation is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__
    * Design materials and license can be found in the `git repository <https://git.beagleboard.org/beagleboard/beaglebone-black>`__
    * Use of the boards or design materials constitutes an agreement to the :ref:`boards-terms-and-conditions`
    * Software images and purchase links available on the `board page <https://www.beagleboard.org/boards/beaglebone-black>`__
    * For export, emissions and other compliance, see :ref:`beagleboneblack-support`

.. image:: images/image1.jpg
   :width: 598
   :align: center
   :alt: BeagleBone Black

.. toctree::
   :maxdepth: 1

   ch01
   ch02
   ch03
   ch04
   ch05
   ch06
   ch07
   ch08
   ch09
   ch10
   ch11

